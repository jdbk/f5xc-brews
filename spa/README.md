# front-end

## Project setup

```shell
npm install
```

### Compiles and hot-reloads for development

```shell
npm run serve
```

### Compiles and minifies for production

```shell
npm run build
```

### Lints and fixes files

```shell
npm run lint
```

### Audit and fix security issues

```shell
npm audit fix
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
